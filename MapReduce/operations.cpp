#include "operations.h"

fileIdAndWordCount countWordsAppearences(char *filename, vector<string> words)
{
	fileIdAndWordCount wordsCount;
	vector<wordAndCount> wordAppereance;
	sort(words.begin(), words.end());

	string word = words[0];
	int wordCount = 1;

	for (size_t i = 1; i < words.size(); ++i)
	{
		if (word != words[i])
		{
			wordAndCount temp;
			temp.word = new char[strlen(word.c_str()) + 1];
			strcpy_s(temp.word, strlen(word.c_str()) + 1, word.c_str());
			temp.count = wordCount;
			wordAppereance.push_back(temp);
			wordCount = 0;
			word = words[i];
		}
		wordCount++;
	}

	wordAndCount temp;
	temp.word = new char[strlen(word.c_str()) + 1];
	strcpy_s(temp.word, strlen(word.c_str()) + 1, word.c_str());
	temp.count = wordCount;
	wordAppereance.push_back(temp);
	wordsCount.filename = new char[strlen(filename) + 1];
	strcpy_s(wordsCount.filename, strlen(filename) + 1, filename);
	wordsCount.words = wordAppereance;

	return wordsCount;
}

vector<fileIdAndWordCount> splitTheList(fileIdAndWordCount wordList)
{
	vector<fileIdAndWordCount> countList;
	for (size_t i = 0; i < wordList.words.size(); ++i)
	{
		fileIdAndWordCount fileWord;
		fileWord.filename = wordList.filename;
		wordAndCount wordCount;
		wordCount.word = wordList.words[i].word;
		wordCount.count = wordList.words[i].count;
		fileWord.words.push_back(wordCount);

		countList.push_back(fileWord);
	}
	return countList;
}

vector<wordFileCount> switchValuesFromList(vector<fileIdAndWordCount> countList)
{
	vector<wordFileCount> newList;
	for (size_t i = 0; i < countList.size(); ++i)
	{
		wordFileCount wordFile;
		wordFile.word = countList[i].words[0].word;

		filenameAndCount fileCount;
		fileCount.filename = countList[i].filename;
		fileCount.count = countList[i].words[0].count;

		wordFile.fileCount = fileCount;

		newList.push_back(wordFile);
	}

	return newList;
}

void sortValues(vector<wordFileCount>& words)
{
	for (size_t i = 0; i < words.size() - 1; ++i)
	{
		for (size_t j = i + 1; j < words.size(); ++j)
		{
			if (strcmp(words[i].word, words[j].word) > 0)
			{
				wordFileCount aux = words[i];
				words[i] = words[j];
				words[j] = aux;
			}
		}
	}

	for (size_t i = 0; i < words.size() - 1; ++i)
	{
		for (size_t j = i + 1; j < words.size() && strcmp(words[i].word, words[j].word) == 0; ++j)
		{
			if (strcmp(words[i].fileCount.filename, words[j].fileCount.filename) > 0)
			{
				wordFileCount aux = words[i];
				words[i] = words[j];
				words[j] = aux;
			}
		}
	}
}

vector<wordFilesConcat> concatFilesForEveryWords(vector<wordFileCount> words)
{
	vector<wordFilesConcat> filesConcat;
	vector<filenameAndCount> filesWord;

	char *word = words[0].word;

	filesWord.push_back(words[0].fileCount);

	for (size_t i = 1; i < words.size(); ++i)
	{
		if (strcmp(word, words[i].word) != 0)
		{
			wordFilesConcat aux;
			aux.word = word;
			aux.files = filesWord;
			filesConcat.push_back(aux);
			filesWord.clear();
			word = words[i].word;
		}
		filesWord.push_back(words[i].fileCount);
	}

	wordFilesConcat aux;
	aux.word = word;
	aux.files = filesWord;
	filesConcat.push_back(aux);

	return filesConcat;
}

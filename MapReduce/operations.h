#pragma once

#include "source.h"

fileIdAndWordCount countWordsAppearences(char *filename, vector<string> words);
vector<fileIdAndWordCount> splitTheList(fileIdAndWordCount wordList);
vector<wordFileCount> switchValuesFromList(vector<fileIdAndWordCount> countList);
void sortValues(vector<wordFileCount>& words);
vector<wordFilesConcat> concatFilesForEveryWords(vector<wordFileCount> words);
#include"source.h"
#include "operations.h"

int main(int argc, char **argv)
{
	int id;
	int size;
	int index[2];
	vector<char *> files;
	vector<string> words;
	vector<wordFileCount> wordsFromAllFiles;

	MPI_Status status;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &id);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	string dir = argv[1];
	dir = dir + "/*";
	files = readFilesName(dir.c_str());
	if (id == 0)
	{
		int nrFisiere = files.size() / size;
		for (int i = 1; i < size - 1; ++i)
		{
			index[0] = i*nrFisiere;
			index[1] = i*nrFisiere + nrFisiere - 1;
			MPI_Send(index, 2, MPI_INT, i, 99, MPI_COMM_WORLD);
		}
		index[0] = (size - 1)*nrFisiere;
		index[1] = files.size() - 1;
		MPI_Send(index, 2, MPI_INT, size - 1, 99, MPI_COMM_WORLD);

		index[0] = id*nrFisiere;
		index[1] = id*nrFisiere + nrFisiere - 1;

		cout << id << "->" << index[0] << " - " << index[1] << endl;

		for (int i = index[0]; i <= index[1]; ++i)
		{
			string file = files[i];
			string temp = argv[1];
			words = readFromFile(temp + "/" + file);
			fileIdAndWordCount wordsCount = countWordsAppearences(files[i], words);
			vector<fileIdAndWordCount> countList = splitTheList(wordsCount);
			vector<wordFileCount> valuesSwitched = switchValuesFromList(countList);
			for (size_t j = 0; j < valuesSwitched.size(); ++j)
			{
				wordsFromAllFiles.push_back(valuesSwitched[i]);
			}
		}

		for (int i = 1; i < size; ++i)
		{
			int recvNr;
			MPI_Recv(&recvNr, 1, MPI_INT, i, 99, MPI_COMM_WORLD, &status);
			for (int j = 0; j < recvNr; ++j)
			{
				int recvWordLen;
				MPI_Recv(&recvWordLen, 1, MPI_INT, i, 99, MPI_COMM_WORLD, &status);
				char *wordRecv = new char[recvWordLen + 1];
				MPI_Recv(wordRecv, recvWordLen + 1, MPI_CHAR, i, 99, MPI_COMM_WORLD, &status);

				int filenameLenRecv;
				MPI_Recv(&filenameLenRecv, 1, MPI_INT, i, 99, MPI_COMM_WORLD, &status);
				char *filename = new char[filenameLenRecv + 1];
				MPI_Recv(filename, filenameLenRecv + 1, MPI_CHAR, i, 99, MPI_COMM_WORLD, &status);

				int countRecv;
				MPI_Recv(&countRecv, 1, MPI_INT, i, 99, MPI_COMM_WORLD, &status);

				wordFileCount w;
				w.word = wordRecv;
				w.fileCount.filename = filename;
				w.fileCount.count = countRecv;

				wordsFromAllFiles.push_back(w);
			}
		}

		sortValues(wordsFromAllFiles);

		vector<wordFilesConcat> wordFilesConcat = concatFilesForEveryWords(wordsFromAllFiles);

		writeResultToFile(wordFilesConcat, argv[2]);
	}
	else
	{
		MPI_Recv(index, 2, MPI_INT, 0, 99, MPI_COMM_WORLD, &status);

		cout << id << "->" << index[0] << " - " << index[1] << endl;

		for (int i = index[0]; i <= index[1]; ++i)
		{
			string file = files[i];
			string temp = argv[1];
			words = readFromFile(temp + "/" + file);
			fileIdAndWordCount wordsCount = countWordsAppearences(files[i], words);
			vector<fileIdAndWordCount> countList = splitTheList(wordsCount);
			vector<wordFileCount> valuesSwitched = switchValuesFromList(countList);
			for (size_t i = 0; i < valuesSwitched.size(); ++i)
			{
				wordsFromAllFiles.push_back(valuesSwitched[i]);
			}
		}

		int nr = wordsFromAllFiles.size();
		MPI_Send(&nr, 1, MPI_INT, 0, 99, MPI_COMM_WORLD);

		for (int i = 0; i < nr; ++i)
		{
			int wordLen = strlen(wordsFromAllFiles[i].word);
			MPI_Send(&wordLen, 1, MPI_INT, 0, 99, MPI_COMM_WORLD);
			MPI_Send(wordsFromAllFiles[i].word, wordLen + 1, MPI_CHAR, 0, 99, MPI_COMM_WORLD);

			int filenameLen = strlen(wordsFromAllFiles[i].fileCount.filename);
			MPI_Send(&filenameLen, 1, MPI_INT, 0, 99, MPI_COMM_WORLD);
			MPI_Send(wordsFromAllFiles[i].fileCount.filename, filenameLen + 1, MPI_CHAR, 0, 99, MPI_COMM_WORLD);

			MPI_Send(&wordsFromAllFiles[i].fileCount.count, 1, MPI_INT, 0, 99, MPI_COMM_WORLD);
		}
	}

	MPI_Finalize();
	return 0;
}
#pragma once

#include<fstream>
#include<sstream>
#include<iostream>
#include<string>
#include<filesystem>
#include<io.h>
#include<algorithm>
#include "mpi.h"
using namespace std;

struct filenameAndCount {
	char *filename;
	int count;
};

struct wordAndCount {
	char *word;
	int count;
};

struct fileIdAndWordCount {
	char *filename;
	vector<wordAndCount> words;
};

struct wordFileCount {
	char *word;
	filenameAndCount fileCount;
};

struct wordFilesConcat {
	string word;
	vector<filenameAndCount> files;
};

string removePunctuation(string word);
vector<char *> readFilesName(const char *path);
vector<string> readFromFile(string path);
void writeResultToFile(vector<wordFilesConcat> words, const char *path);
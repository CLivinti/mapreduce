#include "source.h"

string removePunctuation(string word)
{
	string newWord = "";
	for (size_t i = 0; i < word.length(); ++i)
	{
		if (word[i] >= - 1 && word[i] <= 255 && !ispunct(word[i]) && !isdigit(word[i]))
			newWord += word[i];
	}
	return newWord;
}

vector<char *> readFilesName(const char *path)
{
	vector<char*> files;

	_finddata_t data;
	int ff = _findfirst(path, &data);
	if (ff != -1)
	{
		int res = 0;
		while (res != -1)
		{
			if ((data.attrib & _A_SUBDIR) != _A_SUBDIR) {
				char *temp = new char[strlen(data.name) + 1];
				strcpy_s(temp, strlen(data.name) + 1, data.name);
				files.push_back(temp);
			}
			res = _findnext(ff, &data);
		}
		_findclose(ff);
	}

	return files;
}

vector<string> readFromFile(string path)
{
	ifstream fisier;
	fisier.open(path);
	string word;
	string line;
	vector<string> words;
	while (getline(fisier,line))
	{
		stringstream ssWordBuf(line);
		while (ssWordBuf >> word)
		{
			word = removePunctuation(word);
			transform(word.begin(), word.end(), word.begin(), ::tolower);
			if (word != "") {
				words.push_back(word);
			}
		}
	}
	fisier.close();
	return words;
}

void writeResultToFile(vector<wordFilesConcat> words, const char *path)
{
	ofstream file;
	file.open(path);

	for (size_t i = 0; i < words.size(); ++i)
	{
		cout << words[i].word << " -> {";
		file << words[i].word << " -> {";
		for (size_t j = 0; j < words[i].files.size() - 1; ++j)
		{
			cout << words[i].files[j].filename << ":" << words[i].files[j].count << ",";
			file << words[i].files[j].filename << ":" << words[i].files[j].count << ",";
		}
		cout << words[i].files[words[i].files.size() - 1].filename << ":" << words[i].files[words[i].files.size() - 1].count << "};\n\r";
		file << words[i].files[words[i].files.size() - 1].filename << ":" << words[i].files[words[i].files.size() - 1].count << "};\n\r";
	}

	file.close();
}
